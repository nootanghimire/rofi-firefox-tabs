# rofi-firefox-tabs

This is a work in progress.

Currently working functionality:
* Fetch list of open tabs in main Firefox process and display in Rofi
* Switch to any tab


**Rofi configuration**
```
configuration {
	modi: "window,drun,tabs:python /home/ddl37/Documents/rofi-firefox-tabs/script/tabs.py";
}
```


**Issues**
* Currently uses regular polling (~5s) to get tabs from browser - slow to update (TODO FIX to use request-response)


![current working screenshot](screenshots/progress-screenshot.png)