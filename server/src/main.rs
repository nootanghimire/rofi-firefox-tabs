#[macro_use]
extern crate serde_derive;

mod tcp;
mod webext;
mod tabs;

use std::thread;
use std::sync::mpsc::{channel, Sender, Receiver};
use std::sync::{Mutex, Arc};

type Store = Arc<Mutex<tabs::TabStore>>;

fn main() {
    let (sender, receiver) = channel::<String>();

    let store: Store = Arc::new(Mutex::new(tabs::TabStore::new()));

    let m1 = store.clone();
    let m2 = store.clone();

    let websocket_thread = thread::spawn(move || webext::run("127.0.0.1:8081", m1, receiver));
    let tcp_thread = thread::spawn(move || tcp::run("127.0.0.1:8082", m2, sender.clone()));

    if websocket_thread.join().is_err() || tcp_thread.join().is_err() {
        println!("Failed to exit successfully");
    }
}
