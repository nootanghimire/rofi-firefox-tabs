use ws::{connect, Handler, Sender, Handshake, Result, Error, Message, CloseCode};
use super::*;

struct Server {
    out: Arc<Mutex<Option<Sender>>>,
    store: Store,
    tcp_channel: Option<()>
}

impl Handler for Server {
    fn on_shutdown(&mut self) {
        unimplemented!()
    }

    fn on_open(&mut self, shake: Handshake) -> Result<()> {
        println!("WebSocket :: connection from browser at {}", shake.peer_addr.unwrap().ip());
        Ok(())
    }

    fn on_message(&mut self, msg: Message) -> Result<()> {
        let text = msg.into_text().unwrap();
        let mut summary = text.clone();
        summary.truncate(50);

        let t: Vec<tabs::BrowserTab> = tabs::parse_json(text);

        let mut lock = self.store.lock().unwrap();
        lock.tabs = t; // moved

        Ok(())
    }

    fn on_close(&mut self, code: CloseCode, reason: &str) {

        println!("Socket closed: {}", reason);
        let mut handle = self.out.lock().unwrap();
        *handle = None;
    }

    fn on_error(&mut self, err: Error) {
        println!("Something happened ¯\\_(ツ)_/¯");
        let mut handle = self.out.lock().unwrap();
        *handle = None;
    }
}

pub fn run(address: &str, store: Store, comm: Receiver<String>) {

    let handles: Arc<Mutex<Vec<Arc<Mutex<Option<ws::Sender>>>>>> = Arc::new(Mutex::new(Vec::new()));

    let h2 = handles.clone();
    thread::spawn(move || {
        for response in comm.iter() {
            let hand = h2.lock().unwrap();

            let send_result: Vec<Result<()>> = hand.iter().filter_map(|send_handle| {
                let sender = send_handle.lock().unwrap();
                sender.as_ref().map(|socket| socket.send(response.clone()))
            }).collect();
            
            let successes = send_result.iter().filter(|item| item.is_ok()).count();
            if successes != send_result.len() {
                println!("Sending failed for some items");
            }
        }
    });

    ws::listen(address, move |out| {
        // spawn TCP listening thread
        let locked = Arc::new(Mutex::new(Some(out)));
        handles.lock().unwrap().push(locked.clone());

        Server {out: locked.clone(), store: store.clone(), tcp_channel: None}
    }).unwrap();
}
