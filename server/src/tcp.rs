use std::io::prelude::*;
use std::net::{TcpListener, TcpStream, Shutdown};
use std::str;

use super::*;

fn read_stream(stream: &mut TcpStream) -> String {
    const BUF_SIZE: usize = 1024;

    let mut text = String::new();
    let mut buffer: [u8; BUF_SIZE] = [0; BUF_SIZE];

    loop {
        let bytes_read = stream.read(&mut buffer).unwrap();
        if bytes_read == BUF_SIZE {
            let add = str::from_utf8(&buffer).unwrap();
            text.push_str(add);
        } else {
            let add = str::from_utf8(&buffer[0..bytes_read]).unwrap();
            text.push_str(add);
            break;
        }
    }
    return text;
}

fn handle_client(mut st: TcpStream, store: Store, sender: Sender<String>) -> Result<(), Box<dyn std::error::Error>> {
    let client_request = read_stream(&mut st);
    let tab_store = store.lock().unwrap();

    if client_request == "GET" {
        let json = serde_json::to_string(&*tab_store).expect("Client provided invalid JSON for GET request");
        st.write(json.as_bytes())?;
    }
    else if &client_request[0..3] == "SET" {
        let tab_name = client_request[4..].to_string(); // space after SET

        let matching_tab = tab_store.get_by_name(&tab_name);
        match matching_tab {
            Some(tab) => { sender.send(tab.id.to_string())?; },
            None => println!("The tab with the given ID does not exist")
        }
    } else {
        panic!("Invalid request format");
    }

    st.shutdown(Shutdown::Both)?;
    Ok(())
}

pub fn run(address: &str, store: Store, comm: Sender<String>) -> std::io::Result<()> {
    let listener = TcpListener::bind(address)?;
    for stream in listener.incoming() {
        let stream = stream?;
        // println!("TCP :: connection from {}", stream.local_addr().unwrap());

        match handle_client(stream, store.clone(), comm.clone()) {
            Ok(_) => {}
            Err(e) => { println!("Failed to handle client"); dbg!(e); }
        };
    }

    Ok(())
}
