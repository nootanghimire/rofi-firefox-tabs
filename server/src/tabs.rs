extern crate serde;
extern crate serde_json;

use serde::ser::{Serialize, Serializer, SerializeSeq, SerializeMap};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BrowserTab {
    pub active: bool,
    pub title: String,
    pub url: String,
    pub id: i32,
}

pub struct TabStore {
    pub tabs: Vec<BrowserTab>
}

impl TabStore {
    pub fn new() -> Self {
        TabStore { tabs: Vec::<BrowserTab>::new() }
    }
    pub fn get_by_name(&self, name: &str) -> Option<BrowserTab> {
        let mut matching = self.tabs.iter()
            .filter(|t| { t.title == name })
            .cloned()
            .collect::<Vec<BrowserTab>>();
        
        return match matching.len() {
            0 => None,
            1 => matching.pop(),
            _ => {
                println!("Found multiple tabs with same name: {}", name);
                matching.pop()
            }
        }
    }
}

impl Serialize for TabStore {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where
        S: Serializer {

        let mut seq = serializer.serialize_seq(Some(self.tabs.len()))?;
        for e in self.tabs.iter() {
            seq.serialize_element(e)?;
        }
        seq.end()
    }
}

pub fn parse_json(json: String) -> Vec<BrowserTab> {
    let js: serde_json::Value = serde_json::from_str(&json).unwrap();

    return js.as_array()
        .unwrap()
        .iter()
        .map(|v| serde_json::from_value(v.clone()))
        .filter_map(|b| {
            match b {
                Ok(tab) => Some(tab),
                Err(e) => None
            }
        }).collect::<Vec<BrowserTab>>();
}