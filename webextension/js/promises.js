async function wait(ms) {
    return new Promise((yay, nay) => setTimeout(yay, ms))
}

async function get_websocket(address) {
    let s = new WebSocket(address)

    return new Promise((yay, nay) => {
        s.onopen = () => yay(s)
        s.onclose = nay
        s.onerror = nay
    })
}

function display(p) {
    p.then(out => console.log(out)).catch(err => console.error(err))
}
