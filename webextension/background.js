function websocket_handler(address) {
    let socket = null

    async function get() {
        console.log(`Socket: ${socket}`)
        if (socket !== null && socket.readyState <= 1) {
            return socket
        }
        if (socket !== null) {
            socket.close()
        }

        while (socket === null || !(socket.readyState <= 1)) {
            try {
                socket = await get_websocket(address)
            } catch (err) {
                console.log(`Failed to connect to ${address}`)
                await wait(2000)
                continue
            }

            console.log(`Successfully connected to ${address}`)
            return socket
        }

        throw new Error('!!!')
    }

    return { get }
}

setInterval(() => {
    console.log("Hello world???")
}, 5000)

let handle = websocket_handler("ws://127.0.0.1:8081")

async function main_loop() {
    while (true) {
        let socket = await handle.get()

        socket.onmessage = (msg) => {
            console.log("I got a message!", msg);
            let tab_id = Number.parseInt(msg.data)

            let target = browser.tabs.get(tab_id)
            target.then((tab_info) => {
                browser.tabs.update(tab_id, {
                    active: true
                })
                browser.windows.update(tab_info.windowId, {
                    focused: true,
                    drawAttention: true,
                    // state: "normal"
                })
            }).catch(err => {
                console.error("TAB NOT FOUND - may have been closed")
            })
        }
        console.log(socket)

        let message = (await browser.tabs.query({})).filter(x => x.incognito === false)
        console.log(message)
        socket.send(JSON.stringify(message))

        await wait(5000)
    }
}

display(main_loop())