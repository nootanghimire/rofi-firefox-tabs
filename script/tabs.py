import sys
import os
import socket
import json

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('127.0.0.1', 8082))


def read_socket(s: socket.socket, BUFFER_SIZE=4096):
    buffer = ""
    while True:
        buffer_part = s.recv(BUFFER_SIZE)
        buffer += buffer_part.decode('utf8')

        if len(buffer_part) < BUFFER_SIZE:
            return buffer


def get_tabs():
    client.send('GET'.encode('utf8'))
    response = read_socket(client)
    return json.loads(response)


def set_tab(target):
    client.send(f'SET {target}'.encode('utf8'))


formatter = lambda t: t['title']

if len(sys.argv) > 1:
    set_tab(sys.argv[1])
else:
    tabs = get_tabs()
    print('\n'.join(map(formatter, tabs)))
